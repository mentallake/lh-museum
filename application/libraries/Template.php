<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template{
    var $ci;
    var $js = array();
    var $css = array();

    function __construct(){
        $this->ci =& get_instance();

        $this->init_css();
        $this->init_js();
    }

    private function init_css(){
        // Add base scripts
        // $this->add_css(assets_css_url('reset.min.css'));
        $this->add_css(assets_css_url('animate.min.css'));
        $this->add_css(assets_fonts_url('font-awesome-4.7.0/css/font-awesome.min.css'));
        $this->add_css(assets_libs_url('bootstrap-3.3.7/css/bootstrap.min.css'));
        // $this->add_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
        // $this->add_css('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css');
        $this->add_css(assets_css_url('style.css'));
    }

    private function init_js(){
        // $this->add_js('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js');
        // $this->add_js('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js');
        $this->add_js(assets_libs_url('jquery-3.2.1.min.js'));
        $this->add_js('https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js');
        $this->add_js(assets_libs_url('bootstrap-3.3.7/js/bootstrap.min.js'));
        $this->add_js(assets_libs_url('jquery.touchSwipe.min.js'));

        // Add base scripts
        $this->js[] = '<script type="text/javascript">var APP_NAME = "' . APP_NAME . '";</script>';
        $this->js[] = "<script type='text/javascript'>var app_url = '" . APPPATH . "';</script>";
        $this->js[] = "<script type='text/javascript'>var base_url = '" . base_url() . "';</script>";
        $this->js[] = "<script type='text/javascript'>var site_url = '" . site_url() . "';</script>";
        $this->js[] = "<script type='text/javascript'>var css_url = '" . assets_css_url() . "';</script>";
        $this->js[] = "<script type='text/javascript'>var images_url = '" . assets_images_url() . "';</script>";
        $this->js[] = "<script type='text/javascript'>var libs_url = '" . assets_libs_url() . "';</script>";
        $this->js[] = "<script type='text/javascript'>var scripts_url = '" . assets_scripts_url() . "';</script>";
        $this->js[] = "<script type='text/javascript'>var uploads_url = '" . uploads_url() . "';</script>";

        $this->add_js(assets_scripts_url('js/app.min.js'));
    }

    function load($tpl_view = 'default', $body_view = null, $data = null){
        if ( !is_null( $body_view ) )
        {
            $body_view_fullpath = APPPATH . 'views/front/themes/' . $tpl_view . '/' . $body_view;

            if ( file_exists( $body_view_fullpath ) )
            {
                $body_view_path = 'front/themes/' . $tpl_view . '/' . $body_view;
            }
            else if ( file_exists( $body_view_fullpath . '.php' ) )
            {
                $body_view_path = 'front/themes/' . $tpl_view . '/' . $body_view . '.php';
            }
            else
            {
                show_error('Unable to load the requested file: ' . $tpl_view . '/' . $body_view . '.php');
            }

			$elapsed_time = $this->ci->benchmark->elapsed_time('code_start', 'code_end');
			if ( is_null($data) )
			{
				$data = array('elapsed_time' => $elapsed_time);
			}
			else if ( is_array($data) )
			{
				$data['elapsed_time'] = $elapsed_time;
			}
			else if ( is_object($data) )
			{
				$data->elapsed_time = $elapsed_time;
			}

            $body = $this->ci->load->view($body_view_path, $data, TRUE);

            if ( is_null($data) )
            {
                $data = array('body' => $body);
            }
            else if ( is_array($data) )
            {
                $data['body'] = $body;
            }
            else if ( is_object($data) )
            {
                $data->body = $body;
            }
        }

        // Get Header
        $header_path = 'front/themes/' . $tpl_view . '/shared/header.php';
        $header = $this->ci->load->view($header_path, null, TRUE);

        if ( is_null($data) )
        {
            $data = array('header' => $header);
        }
        else if ( is_array($data) )
        {
            $data['header'] = $header;
        }
        else if ( is_object($data) )
        {
            $data->header = $header;
        }

        // Get Footer
        $footer_path = 'front/themes/' . $tpl_view . '/shared/footer.php';
        $footer = $this->ci->load->view($footer_path, null, TRUE);

        if ( is_null($data) )
        {
            $data = array('footer' => $footer);
        }
        else if ( is_array($data) )
        {
            $data['footer'] = $footer;
        }
        else if ( is_object($data) )
        {
            $data->footer = $footer;
        }

        // Get CSSs
        if ( is_null($data) )
        {
            $data = array('css' => $this->css);
        }
        else if ( is_array($data) )
        {
            $data['css'] = $this->css;
        }
        else if ( is_object($data) )
        {
            $data->css = $this->css;
        }

        // Get Script
        if ( is_null($data) )
        {
            $data = array('js' => $this->js);
        }
        else if ( is_array($data) )
        {
            $data['js'] = $this->js;
        }
        else if ( is_object($data) )
        {
            $data->js = $this->js;
        }

        // Display View
        $layout_path = 'front/themes/' . $tpl_view . '/layout.php';
        $this->ci->load->view($layout_path, $data);
    }

    function add_css($filepath){
        $css = NULL;
        $css = '<link type="text/css" rel="stylesheet" href="'. $filepath .'">';

        // Add to js array if it doesn't already exist
        if ($css != NULL && !in_array($css, $this->css)){
            $this->css[] = $css;
        }
    }

    function add_js($filepath){
        $js = NULL;
        $js = '<script type="text/javascript" src="'. $filepath .'"></script>';

        // Add to js array if it doesn't already exist
        if ($js != NULL && !in_array($js, $this->js))
        {
            $this->js[] = $js;
        }
    }
}

/* End of file template.php */
/* Location: ./application/libraries/template.php */