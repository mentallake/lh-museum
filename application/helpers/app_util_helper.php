<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function convert_date($ddmmyyyy, $time_option = TIME_OPTION_CURRENT){
	$timezone = new DateTimeZone('Asia/Bangkok');
	$date = DateTime::createFromFormat('d/m/Y', $ddmmyyyy, $timezone);

    $format_text = 'Y-m-d H:i:s';

    switch($time_option){
        case TIME_OPTION_START_DAY:
            $format_text = 'Y-m-d 00:00:00';
            break;
        case TIME_OPTION_END_DAY:
            $format_text = 'Y-m-d 23:59:59';
            break;
    }

	return $date->format($format_text);
}

function get_full_date($db_datetime){
    $months = array(
        "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
        "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
        );

    $timezone = new DateTimeZone('Asia/Bangkok');
    $date = DateTime::createFromFormat('Y-m-d H:i:s', $db_datetime, $timezone);

    $month_index = intval($date->format('n')) - 1;
    $month_text = $months[$month_index];

    $year_text = intval($date->format('Y')) + 543;

    return $date->format("d $month_text $year_text H:i:s");
}

function convert_datetime_for_web($db_datetime){
	$timezone = new DateTimeZone('Asia/Bangkok');
	$date = DateTime::createFromFormat('Y-m-d H:i:s', $db_datetime, $timezone);
    $year_text = intval($date->format('Y')) + 543;
	return $date->format("d/m/$year_text H:i:s");
}

function convert_date_for_web($db_date){
    $timezone = new DateTimeZone('Asia/Bangkok');
    $date = DateTime::createFromFormat('Y-m-d', $db_date, $timezone);
    $year_text = intval($date->format('Y')) + 543;
    return $date->format("d/m/$year_text");
}

function convert_datetime_to_date_for_web($db_datetime){
    $timezone = new DateTimeZone('Asia/Bangkok');
    $date = DateTime::createFromFormat('Y-m-d H:i:s', $db_datetime, $timezone);
    $year_text = intval($date->format('Y')) + 543;
    return $date->format("d/m/$year_text");
}

function format_currency($number){
	return number_format($number, 2);
}

function second_to_time_format($seconds){
    $sec_num = intval($seconds);
    $hours   = floor($sec_num / 3600);
    $minutes = floor(($sec_num % 3600) / 60);
    $seconds = floor($sec_num % 60);

    if ($hours   < 10) { $hours   = "0" . $hours;}
    if ($minutes < 10) { $minutes = "0" . $minutes;}
    if ($seconds < 10) { $seconds = "0" . $seconds;}

    return $hours . ':' . $minutes . ':' . $seconds;
}

function time_to_seconds($hour, $min, $second){
    return ($hour * 60 * 60) + ($min * 60) + $second;
}

function send_email($to, $subject, $message, $mail_type, $sender = array("No Reply [" . APP_NAME . "]", "noreply@vlivingpro.com")) {
	$CI =& get_instance();
	$CI->load->library('email');
	$CI->email->initialize(array(
		"mailtype" => "html"
	));
	$CI->email->clear(); // Don't Forget to call this before you want to send email.
	$CI->email->from($sender[1], $sender[0]);
	$CI->email->to($to);

	$CI->email->subject($subject);
	$CI->email->message($message);

	$CI->email->send(FALSE);
	$log = $CI->email->print_debugger();

	$insert_data = array(
		"email" => $to,
		"type" => $mail_type,
		"log" => $log
	);
	$CI->db->insert("mail_log", $insert_data);
}

function base64_url_encode($input) {
 return strtr(base64_encode($input), '+/=', '-_,');
}

function base64_url_decode($input) {
 return base64_decode(strtr($input, '-_,', '+/='));
}

if (!function_exists('haversineGreatCircleDistance')) {
	/**
	 * Calculates the great-circle distance between two points, with
	 * the Haversine formula.
	 * @param float $latitudeFrom Latitude of start point in [deg decimal]
	 * @param float $longitudeFrom Longitude of start point in [deg decimal]
	 * @param float $latitudeTo Latitude of target point in [deg decimal]
	 * @param float $longitudeTo Longitude of target point in [deg decimal]
	 * @param float $earthRadius Mean earth radius in [m]
	 * @return float Distance between points in [m] (same as earthRadius)
	 */
	function haversineGreatCircleDistance(
	  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
	{
	  // convert from degrees to radians
	  $latFrom = deg2rad($latitudeFrom);
	  $lonFrom = deg2rad($longitudeFrom);
	  $latTo = deg2rad($latitudeTo);
	  $lonTo = deg2rad($longitudeTo);

	  $latDelta = $latTo - $latFrom;
	  $lonDelta = $lonTo - $lonFrom;

	  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
	  return $angle * $earthRadius;
	}
}

if (!function_exists('vincentyGreatCircleDistance')) {
	/**
	 * Calculates the great-circle distance between two points, with
	 * the Vincenty formula.
	 * @param float $latitudeFrom Latitude of start point in [deg decimal]
	 * @param float $longitudeFrom Longitude of start point in [deg decimal]
	 * @param float $latitudeTo Latitude of target point in [deg decimal]
	 * @param float $longitudeTo Longitude of target point in [deg decimal]
	 * @param float $earthRadius Mean earth radius in [m]
	 * @return float Distance between points in [m] (same as earthRadius)
	 */
	function vincentyGreatCircleDistance(
	  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
	{
	  // convert from degrees to radians
	  $latFrom = deg2rad($latitudeFrom);
	  $lonFrom = deg2rad($longitudeFrom);
	  $latTo = deg2rad($latitudeTo);
	  $lonTo = deg2rad($longitudeTo);

	  $lonDelta = $lonTo - $lonFrom;
	  $a = pow(cos($latTo) * sin($lonDelta), 2) +
		pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
	  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

	  $angle = atan2(sqrt($a), $b);
	  return $angle * $earthRadius;
	}
}

if (!function_exists('youtube_video_id')) {
	function youtube_video_id($url  = "") {
		parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
		return $my_array_of_vars["v"];
	}
}

if (!function_exists('add_notification')) {
	function add_notification($from, $to, $type, $message) {
		$CI =& get_instance();
		$CI->load->database();

		$obj = array(
			"type" => $type,
			"message" => $message
		);

		$json = json_encode($obj);

		$data = array(
			"f" => $from,
			"t" => $to,
			"obj" => $json
		);

		$CI->db->insert("notification", $data);
	}
}

if (!function_exists('retrive_notif')) {
	function retrive_notif($user_id, $unseen_only = false) {
		$CI =& get_instance();
		$CI->load->database();

		$CI->db->where('t', $user_id);
		$CI->db->order_by('created_time', 'desc');

		if ($unseen_only) {
			$CI->db->where('seen', 0);
		}

		$query = $CI->db->get("notification");
		return $query->result_array();
	}
}

if (!function_exists('seen_notification')) {
	function seen_notification($ids = array()) {
		$CI =& get_instance();
		$CI->load->database();

		if (count($ids) > 0) {
			$data = array(
				"seen" => 1
			);

			$CI->db->where_in("id", $ids);
			$CI->db->update("notification", $data);
		}
	}
}

if (!function_exists('image_fix_orientation')) {
	function image_fix_orientation($path){
return true;
	    $image = imagecreatefromjpeg($path);
	    $exif = exif_read_data($path);

	    if (empty($exif['Orientation'])){
	        return false;
	    }

	    switch ($exif['Orientation']){
	        case 3:
	            $image = imagerotate($image, 180, 0);
	            break;
	        case 6:
	            $image = imagerotate($image, - 90, 0);
	            break;
	        case 8:
	            $image = imagerotate($image, 90, 0);
	            break;
	    }

	    imagejpeg($image, $path);

	    return true;
	}
}

if (!function_exists("bd_nice_number")) {
	function bd_nice_number($n) {
		// first strip any formatting;
		$n = (0+str_replace(",","",$n));

		// is this a number?
		if(!is_numeric($n)) return false;

		// now filter it;
		if($n>1000000000000) return round(($n/1000000000000),1).' t';
		else if($n>1000000000) return round(($n/1000000000),1).' b';
		else if($n>1000000) return round(($n/1000000),1).' m';
		else if($n>1000) return round(($n/1000),1).' k';

		return number_format($n);
	}
}