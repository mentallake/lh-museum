<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Controller extends CI_Controller {
	var $ci;
    var $title = APP_NAME;
    var $menu = MENU_NONE;
    var $data = array();
    var $template_name = 'default';
    var $logged_in_user_info = false;
    var $is_user_logged_in = false;

    /**
	 * Constructor
	 */
    public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');

		// $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		// $this->benchmark->mark('code_start');

        $this->ci =& get_instance();
		// $this->ci->output->cache($this->ci->config->item("caching_view_time"));

		// $this->load->helper("cookie");

        // $logged_in_user_info = $this->session->userdata('app_logged_in_user_info');

		// if (! $logged_in_user_info) {
		// 	$remember_me_token = get_cookie('remember_me_token');
		// 	if ($remember_me_token) {
		// 		$this->load->model('account_model');
		// 		$account = $this->account_model->get_account_by_remember_me_token($remember_me_token);
		// 		$this->save_user_info_in_session($account, false /* for not force re generate token */);
		// 	}
		// }

        // if($logged_in_user_info){
        //     $this->logged_in_user_info = $logged_in_user_info;
        //     $this->is_user_logged_in = true;

        //     $this->update_user_info_session();
        // }

        $session_expiration = $this->config->item('sess_expiration');

        $data = array(
            "title" => $this->title,
            "menu" => $this->menu,
            "user_info" => $this->logged_in_user_info,
            "is_user_logged_in" => $this->is_user_logged_in,
            "session_expiration" => $session_expiration,
        );
        $this->add_data($data);
    }

	function add_data($data){
        $data_keys = array_keys($data);
        $data_values = array_values($data);

        for($i = 0; $i < count($data_keys); $i++){
            $this->data[$data_keys[$i]] = $data_values[$i];
        }
    }

    public function do_login($user_info){
    }

    public function save_user_info_in_session($account, $remember_me = FALSE){
    }

    function update_user_info_session(){
    }

    function do_logout(){
    }

    function verify_if_login(){
        if(!$this->is_user_logged_in){
            redirect('home');
        }
    }

    function verify_if_admin_login(){
        if(!$this->is_user_logged_in){
            redirect('admin/login');
        }
    }
}

/* End of file app_controller.php */
/* Location: ./application/core/app_controller.php */