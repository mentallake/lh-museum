<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index(){
		$data = array();

		// Caching view page for 2 hours
		// $this->output->cache(120);
		// $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');

		$this->add_data($data);
		$this->load->library('template');
		// $this->template->add_css(assets_libs_url('fullpage/jquery.fullpage.min.css'));
		// $this->template->add_js(assets_libs_url('fullpage/jquery.fullpage.min.js'));
		$this->template->add_css('https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.css');
		$this->template->add_js('https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.js');
		$this->template->add_js(assets_libs_url('jquery.horizonScroll.js'));
		$this->template->add_js(assets_scripts_url('js/home.js'));
		$this->template->load($this->template_name, 'home_view', $this->data);
	}

	public function get_images(){
		$all_images = 503;
		$images_info = array();

		for($i = 0; $i < $all_images; $i++){
			$item = array();
			$image_no = sprintf('%04d', $i);
			$item['name'] = "180727_Museum_" . $image_no . ".jpg";

			$images_info[] = $item;
		}

		$result = true;
		$message = "";

		$response = array(
			"result" => $result,
			"message" => $message,
			"data" => $images_info,
		);

		echo json_encode($response);
	}

	public function php_info(){
		phpinfo();
	}
}
