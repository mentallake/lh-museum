<div id="home-page">
	<div id="logo" class="hide"><img src="<?php echo assets_images_url('logo.png'); ?>" alt=""></div>

	<a class="hide"
	   id="main-menu-btn"
	   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'Click', eventLabel: 'Hamburger'});">
	   	<img src="<?php echo assets_images_url('menu-icon.png'); ?>" alt="">
	</a>
	<div id="menu-panel" class="hide">
		<a id="close-menu-btn"><img src="<?php echo assets_images_url('close-icon.png'); ?>" alt=""></a>

		<div id="alert-message">
			<i class="fa fa-warning"></i>&nbsp;&nbsp;Animation is playing,<br>please wait for a moment.
		</div>

		<ul id="menus">
			<li>
				<a class="menu-item-btn"
				   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'create a better living room'});"
				   data-target="1">
					<img src="<?php echo assets_images_url('logo-white.png'); ?>" alt="">
				</a>
			</li>
			<li class="hide">
				<a class="menu-item-btn"
				   data-target="2">
					<div class="menu-icon"><img src="<?php echo assets_images_url('concept-icon.png'); ?>" alt=""></div>
					<span class="menu-text">
						<div class="subtitle">Create</div>
						<div class="title">Concept</div>
					</span>
				</a>
			</li>
			<li>
				<a class="cf"
				   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'health room'});"
				   data-target="8">
					<div class="menu-icon"><img src="<?php echo assets_images_url('health-icon.png'); ?>" alt=""></div>
					<span class="menu-text">
						<div class="subtitle">Create a better</div>
						<div class="title">Health</div>
					</span>
				</a>
			</li>
			<li>
				<a class="menu-item-btn"
				   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'convenience room'});"
				   data-target="10">
					<div class="menu-icon"><img src="<?php echo assets_images_url('convenience-icon.png'); ?>" alt=""></div>
					<span class="menu-text">
						<div class="subtitle">Create a better</div>
						<div class="title">Convenience</div>
					</span>
				</a>
			</li>
			<li>
				<a class="menu-item-btn"
				   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'safety room'});"
				   data-target="12">
					<div class="menu-icon"><img src="<?php echo assets_images_url('security-icon.png'); ?>" alt=""></div>
					<span class="menu-text">
						<div class="subtitle">Create a better</div>
						<div class="title">Safety & Security</div>
					</span>
				</a>
			</li>
			<li id="lh-menu">
				<a data-href="https://www.lh.co.th" id="lh-logo">
					<!-- <img src="<?php echo assets_images_url('lh-logo.png'); ?>" alt=""> -->
					Back to www.lh.co.th
				</a>
			</li>
		</ul>

		<ul id="social-list">
			<li>
				<a id="share-fb-btn"><img src="<?php echo assets_images_url('fb-icon.png'); ?>" alt=""></a>
			</li>
			<li>
				<a id="share-tw-btn"><img src="<?php echo assets_images_url('tw-icon.png'); ?>" alt=""></a>
			</li>
			<li>
				<a id="share-line-btn" target="_blank" href="https://social-plugins.line.me/lineit/share?url=https://line.me/en"><img src="<?php echo assets_images_url('line-icon.png'); ?>" alt=""></a>
			</li>
		</ul>
	</div>

	<div id="scroll-icon" class="blink hide">
		<img src="<?php echo assets_images_url('scroll.png'); ?>" alt="">
	</div>

	<div id="enter-museum-panel"
	     style="background-image: url(<?php echo uploads_url('sq-images/180727_Museum_0001.jpg'); ?>);">
		<a id="enter-btn"
		   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'enter'});"
		   class="btn">Enter Website</a>

		<div class="progress-panel">
			<div class="progress" style="">
	            <div class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
	                <span class="sr-only">0% Complete</span>
	            </div>
	        </div>
	        <div class="progress-text">
            	0%
            </div>
	    </div>
	</div>

	<div>
		<input type="hidden" id="current-scene" value="0">
		<input type="hidden" id="current-frame" value="0">

		<div id="showroom-container">
			<ul id="room-menus">
				<li>
					<a class="menu-item-btn"
					   data-target="1">
					</a>
				</li>
				<li>
					<a class="menu-item-btn"
					   data-target="4">
					</a>
				</li>
				<li>
					<a class="menu-item-btn"
					   data-target="8">
					</a>
				</li>
				<li>
					<a class="menu-item-btn"
					   data-target="10">
					</a>
				</li>
				<li>
					<a class="menu-item-btn"
					   data-target="12">
					</a>
				</li>
			</ul>

			<div class="nav-container">
				<a class="nav-btn prev"><i class="fa fa-caret-left"></i>&nbsp;&nbsp;<span class="text">Previous</span></a>
				<a class="nav-btn next"><span class="text">Next</span>&nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
			</div>

			<div id="scene-container">
				<canvas id="canvas" class=""></canvas>

				<div id="text-2-1" class="scene-text text-center" data-start="2" data-end="2">
					<div class="title text-center">CREATE a better living</div>
					<div class="desc text-center">
						<p class="">
							เพราะทุกคนในบ้านมีส่วนทำให้บ้านแข็งแรง และเต็มไปด้วยความสุข...เพียงแค่คุณหันกลับมามองกัน ให้เวลาและใส่ใจความรู้สึกของคนในครอบครัว แค่นี้ชีวิตดีๆ ก็เกิดขึ้นได้
						</p>
					</div>
					<!-- <img src="<?php echo uploads_url('living-text.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('living-text-mb.png'); ?>" class="visible-xs"> -->
				</div>

				<div id="text-3-1" class="scene-text" data-start="3" data-end="3">
					<div class="title">CREATE a better living</div>
					<div class="desc">
						<p>
							LAND&HOUSES ขอร่วมเป็นส่วนหนึ่งในการพัฒนา และออกแบบให้ทุกองค์ประกอบของบ้านตอบสนองความต้องการของชีวิตอย่างเข้าใจความเป็นอยู่ที่แท้จริง ด้วยแนวคิด CREATE A BETTER LIVING เพื่อให้ “บ้าน” ทุกหลังเป็นรากฐานให้ทุกครอบครัวมีคุณภาพชีวิตที่ดียิ่งขึ้น และมีความสุขในทุกๆ วัน
						</p>
					</div>
					<!-- <img src="<?php echo uploads_url('living-text.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('living-text-mb.png'); ?>" class="visible-xs"> -->
				</div>

				<div id="text-4-1" class="scene-text" data-start="4" data-end="4">
					<div class="title">CREATE a better <br class="visible-xs">living</div>
					<div class="desc hide">
						<p class="">
							เพราะทุกคนในบ้านมีส่วนทำให้บ้านแข็งแรง และเต็มไปด้วยความสุข...เพียงแค่คุณหันกลับมามองกัน ให้เวลาและใส่ใจความรู้สึกของคนในครอบครัว แค่นี้ชีวิตดีๆ ก็เกิดขึ้นได้
						</p>
						<p>
							LAND&HOUSES ขอร่วมเป็นส่วนหนึ่งในการพัฒนา และออกแบบให้ทุกองค์ประกอบของบ้านตอบสนองความต้องการของชีวิตอย่างเข้าใจความเป็นอยู่ที่แท้จริง ด้วยแนวคิด CREATE A BETTER LIVING เพื่อให้ “บ้าน” ทุกหลังเป็นรากฐานให้ทุกครอบครัวมีคุณภาพชีวิตที่ดียิ่งขึ้น และมีความสุขในทุกๆ วัน
						</p>
					</div>
					<!-- <img src="<?php echo uploads_url('living-text.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('living-text-mb.png'); ?>" class="visible-xs"> -->
				</div>

				<div id="text-5-1" class="scene-text" data-start="5" data-end="5">
					<div class="title">Create a better<br>health</div>
					<div class="desc">
						เพราะสุขภาพคือจุดเริ่มต้นของทุกเรื่องในชีวิต
					</div>
					<!-- <img src="<?php echo uploads_url('health-text.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('health-text-mb.png'); ?>" class="visible-xs"> -->
				</div>

				<div id="text-6-1" class="scene-text" data-start="6" data-end="6">
					<div class="title">Create A Better<br>Convenience</div>
					<div class="desc">
						เพราะบ้านที่ดีต้องทำให้ชีวิตสะดวกสบาย
					</div>
					<!-- <img src="<?php echo uploads_url('convenience-text.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('convenience-text-mb.png'); ?>" class="visible-xs"> -->
				</div>

				<div id="text-7-1" class="scene-text" data-start="7" data-end="7">
					<div class="title">Create A Better<br>Safety & Security</div>
					<div class="desc">
						เพราะความปลอดภัยในบ้านทำให้มีความสุข
					</div>
					<!-- <img src="<?php echo uploads_url('safe-text.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('safe-text-mb.png'); ?>" class="visible-xs"> -->
				</div>

				<div id="text-8-1" class="scene-text" data-start="8" data-end="8">
					<div class="text-content">
						<div class="title">Create a better health</div>
						<div class="desc">
							คิด และสร้างสรรค์รายละเอียดต่างๆ เพื่อสุขภาพที่ดีของผู้อยู่อาศัย<br> ทุกครอบครัวให้มีคุณภาพชีวิตที่ดีอย่างยั่งยืน
						</div>
					</div>
					<!-- <img src="<?php echo uploads_url('health-text-2.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('health-text-2-mb.png'); ?>" class="visible-xs-inline-block"> -->
				</div>

				<div id="text-10-1" class="scene-text" data-start="10" data-end="10">
					<div class="text-content">
						<div class="title">Create a better convenience</div>
						<div class="desc">
							ทุกรายละเอียดถูกคิด และพัฒนามาเพื่อตอบรับพฤติกรรม การอยู่อาศัยจริงของลูกบ้าน เพื่อสร้างให้เกิดความสะดวกสบาย ไม่เป็นภาระให้กับทุกชีวิต และทุกช่วงวัย
						</div>
					</div>
					<!-- <img src="<?php echo uploads_url('convenience-text-2.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('convenience-text-2-mb.png'); ?>" class="visible-xs-inline-block"> -->
				</div>

				<div id="text-12-1" class="scene-text" data-start="12" data-end="12">
					<div class="text-content">
						<div class="title">Create a better safety & security</div>
						<div class="desc">
							คิดรอบด้านอย่างรอบคอบ เพื่อความเป็นอยู่ที่ปลอดภัย และอุ่นใจตลอดไป
						</div>
					</div>
					<!-- <img src="<?php echo uploads_url('safe-text-2.png'); ?>" class="hidden-xs">
					<img src="<?php echo uploads_url('safe-text-2-mb.png'); ?>" class="visible-xs-inline-block"> -->
				</div>

				<section id="scene-1" class="scene">
				</section>

				<section id="scene-2" class="scene">
				</section>

				<section id="scene-3" class="scene">
				</section>

				<section id="scene-4" class="scene">
				</section>

				<section id="scene-5" class="scene">
				</section>

				<section id="scene-6" class="scene">
				</section>

				<section id="scene-7" class="scene">
				</section>

				<section id="scene-8" class="scene">
				</section>

				<section id="scene-9" class="scene">
					<a id="info-9-1"
					   class="info-icon blink hide"
					   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'info health room'});"
					   data-scene="health"
					   data-animate-in="slideInRight"
					   data-position-x="75%"
					   data-position-y="25%">
					   	<img src="<?php echo assets_images_url('info-point.png'); ?>" alt="">
					</a>
				</section>

				<section id="scene-10" class="scene">
				</section>

				<section id="scene-11" class="scene">
					<a id="info-11-1"
					   class="info-icon blink hide"
					   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'info convenience room'});"
					   data-scene="convenience"
					   data-animate-in="slideInLeft"
					   data-position-x="75%"
					   data-position-y="25%">
					   	<img src="<?php echo assets_images_url('info-point.png'); ?>" alt="">
					</a>
				</section>

				<section id="scene-12" class="scene">
				</section>

				<section id="scene-13" class="scene">
					<a id="info-13-1"
					   class="info-icon blink hide"
					   onclick="ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'info safety room'});"
					   data-scene="safety"
					   data-animate-in="slideInLeft"
					   data-position-x="75%"
					   data-position-y="25%">
					   	<img src="<?php echo assets_images_url('info-point.png'); ?>" alt="">
					</a>
				</section>

				<section id="scene-14" class="scene">
				</section>

				<!-- <section id="scene-10" class="scene">
				</section> -->

				<!-- <section id="scene-11" class="scene">
					<a id="info-11-1"
					   class="info-icon blink hide"
					   data-scene="safety"
					   data-animate-in="slideInLeft"
					   data-position-x="79%"
					   data-position-y="50%">
					   	<img src="<?php echo assets_images_url('info-point.png'); ?>" alt="">
					</a>
				</section> -->

				<!-- <section id="scene-12" class="scene">
				</section> -->
			</div>

			<div class="popup-scroll-icon">
				<img src="<?php echo assets_images_url('scroll-icon.png'); ?>" alt="" class="hidden-xs">
				<img src="<?php echo assets_images_url('touch-icon.png'); ?>" alt="" class="visible-xs-inline-block">
			</div>

			<div id="begin-scroll-icon" class="popup-scroll-icon">
				<div class="div hidden-xs">
					<div class="text">Scroll down to begin</div>
					<img src="<?php echo assets_images_url('scroll-down-icon.png'); ?>" alt="" class="hidden-xs">
				</div>
				<div class="visible-xs-inline-block">
					<div class="text">Swipe up to begin</div>
					<img src="<?php echo assets_images_url('touch-up-icon.png'); ?>" alt="" class="visible-xs-inline-block">
				</div>
			</div>

			<div id="image" class="hide"></div>

			<textarea id="console-panel" rows="15"></textarea>
		</div>
	</div>
</div>

<div id="point-info-dialog" class="modal fade fullscreen" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<img src="<?php echo assets_images_url('logo.png'); ?>" alt="" id="logo" class="hide">
			<button type="button" class="close black" data-dismiss="modal" aria-label="Close"></button>

			<div id="info-content-panel" class="container-fluid">
			</div>
		</div>
	</div>
	<div id="popup-scroll-icon" class="">
	</div>

	<a data-href="https://www.lh.co.th/landing/cbl" id="contact-btn">
		<img src="<?php echo assets_images_url('mail-icon.png'); ?>" alt=""> สอบถามข้อมูลเพิ่มเติม
	</a>
</div>

<div id="convenience-popup-content" class="hide">
	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('convenience-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('convenience-1.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">บ้านสร้างเสร็จก่อนขาย เข้าอยู่ได้ไม่ต้องรอ</div>
						<div class="desc">
							<p>
								สร้างบ้านให้แล้วเสร็จก่อนขาย เพื่อผู้ซื้อได้เห็นบ้านหลังจริง บนที่ดินแปลงจริงไม่ต้องจินตนาการ พร้อมจัดสวนสวยรอบบ้าน ได้สัมผัสบ้านทั้งภายนอกและภายใน ตกแต่งและติดตั้ง พื้น ห้องน้ำ ปั๊มน้ำ ถังสำรองน้ำ เห็นขนาดจริงของพื้นที่แต่ละห้องที่คำนวณระยะให้สามารถจัดเฟอร์นิเจอร์ได้อย่างลงตัว ให้คุณสะดวกในการเลือกบ้านหลังที่ถูกใจ และเข้าอยู่อย่างสะดวกสบายไม่ต้องเสี่ยง

							</p>
							<p>
	 							สังคมสร้างเสร็จสมบูรณ์ พร้อมใช้ชีวิตได้ทันที ไม่เพียงแต่ตัวบ้านที่สร้างเสร็จก่อนขาย แต่เรายังสร้างพื้นที่ส่วนกลางให้เสร็จพร้อมใช้บริการ ทั้งสภาพโครงการโดยรวมที่เรียบร้อยสวยงาม สวนส่วนกลางที่ร่มรื่น สโมสรครบสิ่งอำนวยความสะดวก สระว่ายน้ำแยกสระเด็ก-ผู้ใหญ่ ฟิตเนส ซาวน่า สนามเด็กเล่น ฯลฯ ที่สามารถใช้บริการได้ทันทีตั้งแต่วันแรกที่เข้าอยู่
							</p>
						</div>
						<!-- <img src="<?php echo uploads_url('convenience-popup-1.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('convenience-popup-1-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('convenience-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('convenience-2.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">เตรียมความพร้อมให้บ้าน เริ่มอยู่อาศัยได้ทันที </div>
						<div class="desc">
							<ul>
								<li>เตรียมงานระบบต่างๆ ทั้งไฟฟ้า ประปา เพื่อให้สะดวกในการติดตั้งอุปกรณ์เครื่องใช้ในการอำนวยความสะดวกต่างๆ อาทิ เดินสายเครื่องทำน้ำอุ่น เครื่องซักผ้าเครื่องปรับอากาศ เดินสายโทรศัพท์ สัญญาณอินเทอร์เน็ต แบ่งเฟสสายไฟแยกเครื่องหมายชัดเจน สะดวก และง่ายต่อการดูแลรักษา</li>
								<li>ระบบกำจัดปลวกใต้อาคาร วางท่อ และงานระบบตั้งแต่งานโครงสร้างบ้าน</li>
								<li>ที่เก็บขยะริมรั้วออกแบบให้สะดวก และง่ายต่อการใช้งาน มีความสวยงาม และเป็นระเบียบ</li>
								<li>เตรียมข้อมูลแบบ และแปลนของบ้าน พร้อมรายละเอียด Spec วัสดุที่ใช้ งานฐานราก งานโครงสร้าง และงานระบบต่างๆ เพื่อความสะดวกของเจ้าของบ้านในการดูแลรักษา</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('convenience-popup-2.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('convenience-popup-2-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('convenience-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('convenience-3.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">เบาใจด้วยทีมงานขนย้ายมืออาชีพ</div>
						<div class="desc">
							<p>
								เตรียมพร้อมทีมงานขนย้ายมืออาชีพ ไว้อำนวยความสะดวกให้ลูกบ้าน ขนย้ายของจากบ้านหลังเดิมเข้าบ้าน LH หลังใหม่ ทำให้คุณเบาใจเรื่องขนย้าย และประทับใจกับคุณภาพบริการ
							</p>
						</div>
						<!-- <img src="<?php echo uploads_url('convenience-popup-3.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('convenience-popup-3-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('convenience-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('convenience-4.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">อุ่นใจกับบริการหลังการขาย</div>
						<div class="desc">
							<p>
								เพื่อให้บ้านคงคุณภาพ และสวยงามต่อๆ ไป บริการหลังการขายจึงเป็นสิ่งสำคัญที่เราใส่ใจ ปล่อยปัญหาเรื่องบ้านให้เราดูแล ด้วย Service Center ที่ทำงานทุกวัน รับแจ้งปัญหาตลอด 24 ชม. พร้อมบริการต่อไปนี้
							</p>
							<ul>
								<li>Home Check Up Program บริการตรวจสุขภาพบ้านเมื่อครบ 6 เดือนของการอยู่อาศัย</li>
								<li>เก็บงานบ้านเมื่อครบ 1 ปี ดูแลแก้ไขปัญหาของบ้าน และเก็บความเรียบร้อยพร้อมทาสีบ้านให้ใหม่</li>
								<li>แจ้งซ่อมออนไลน์ได้ตลอด 24 ชม. หรือติดต่อ Service Center โทร 1198 กด 2</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('convenience-popup-4.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('convenience-popup-4-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="health-popup-content" class="hide">
	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('health-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('health-1.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">พื้นที่สีเขียวขนาดใหญ่ที่ให้ทั้งออกซิเจนและร่มเงา</div>
						<div class="desc">
							<p>
								การวางผังโครงการที่ออกแบบจัดเตรียมพื้นที่สีเขียวทั้งสวนส่วนกลาง และบริเวณตัวบ้าน ด้วยหลักการคัดเลือกต้นไม้ที่ให้ทั้งความสวยงามร่มรื่น และประโยชน์ใช้สอย รวมถึงขนาดของต้นไม้ใหญ่ที่ไม่เพียงพร้อมให้ร่มเงา แต่ยังสามารถผลิตออกซิเจนได้ในปริมาณที่มาก เพื่อรองรับทุกชีวิตให้สดชื่น สุขภาพดี เมื่ออยู่ท่ามกลางธรรมชาติ
							</p>
						</div>
						<!-- <img src="<?php echo uploads_url('health-popup-1.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('health-popup-1-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('health-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('health-2.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">เพิ่มอากาศใหม่ให้บ้านหายใจได้ทุกวัน</div>
						<div class="desc">
							<p>
								อากาศแม้มองไม่เห็นแต่มีความสำคัญ กำจัดอากาศเก่าในบ้านที่ทำให้เกิดความอบอ้าว ด้วยนวัตกรรม Air Plus เทคโนโลยีถ่ายเทอากาศ ที่จะนำอากาศใหม่เข้ามาแทนที่อากาศเก่า เกิดภาวะสบายในบ้าน เพราะบ้านหายใจได้เอง คุณ และครอบครัวก็หายใจได้เต็มปอดเช่นกัน
							</p>
						</div>
						<!-- <img src="<?php echo uploads_url('health-popup-2.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('health-popup-2-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('health-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('health-3.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">สระว่ายน้ำที่ดีต่อสุขภาพ</div>
						<div class="desc">
							<p>
								สระว่ายน้ำระบบเกลือ คือ อีกระบบของสระว่ายน้ำที่คำนึงถึงผู้ใช้งาน น้ำที่บำบัดด้วยระบบเกลืออ่อนโยนต่อสุขภาพผิว ผม และฟัน ไม่ก่อให้เกิดการระคายเคืองเหมือนสารคลอรีนแบบเดิม ทำให้ผู้ใช้ทุกวัยได้สุขภาพที่ดีจากการว่ายน้ำ โดยไม่ต้องกังวลเรื่องสารเคมีอีกต่อไป
							</p>
						</div>
						<!-- <img src="<?php echo uploads_url('health-popup-3.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('health-popup-3-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('health-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('health-4.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">บ้านอยู่สบายหลบแดดได้ลม</div>
						<div class="desc">
							เมื่อบ้านอยู่สบาย สมาชิกในบ้านก็สุขกายสบายใจ
							<ul>
								<li>ออกแบบ และวางแปลนบ้านโดยคำนึงถึงทิศของแดด และลม ให้ลงตัวกับฟังก์ชั่นการใช้ งานภายในบ้าน บ้านจึงถูกวางในทิศที่อยู่แล้วสบาย หลบแดดแต่ได้ลม</li>
								<li>บ้านไม่ร้อน ด้วยกระจกเขียวกันความร้อน ตัดกรองรังสี UV ช่วยลดความร้อนจากแสงแดดที่ส่องเข้ามาในบ้าน พร้อมแผ่นสะท้อนความร้อนใต้หลังคา สะท้อนความร้อนที่ส่งมายังตัวบ้านโดยตรง</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('health-popup-4.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('health-popup-4-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="safety-popup-content" class="hide">
	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-1.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">เครื่องตัดกระแสไฟฟ้าอัตโนมัติ</div>
						<div class="desc">
							<p>
								ทุกรายละเอียดของบ้านถูกคิดจากความใส่ใจ ด้วยคำนึงถึงการใช้ทุกช่วงของชีวิต ให้อยู่ได้อย่างปลอดภัย และมีความสุข
							</p>
							<ul>
								<li>พื้นบ้านปลอดภัย เลือกความต่างของพื้นภายในบ้านให้พื้นผิวเรียบทำความสะอาดได้ง่าย ส่วนพื้นภายนอก หรือพื้นบ้านภายในที่ต้องเปียกจากการใช้งาน เลือกพื้นผิวไม่เรียบ เพื่อกันการลื่นล้ม</li>
								<li>ห้องน้ำอุ่นใจ ดีไซน์พื้นที่แบ่งส่วนเปียก-แห้งในห้องน้ำ ใช้พื้นผิวกันลื่นในส่วนเปียก หรือส่วนอาบน้ำ  และพื้นผิวทำความสะอาดง่ายไม่ลื่นในส่วนแห้ง พร้อม Bath Seat ที่นั่งอาบน้ำดีไซน์เฉพาะ อำนวยความสะดวกในการนั่งอาบน้ำ ทั้งยังสามารถรองรับน้ำหนักได้มากป้องกันการลื่นล้ม  มีฝักบัวพร้อมราวปรับระดับความสูงต่ำ สะดวกต่อการใช้งาน</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-1.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('safe-popup-1-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-2.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">ความปลอดภัยตั้งแต่ภายในบ้าน</div>
						<div class="desc">
							<ul>
								<li>ระบบไฟปลอดภัย มีการวางสายไฟจัดระเบียบแยกชุดสีตามจุดใช้งาน เพื่อความสะดวกในการดูแลรักษา พร้อมระบบป้องกันอันตรายด้วยระบบตัดไฟแบบ RCBO ที่ได้มาตรฐานระดับโลก เมื่อเกิดไฟฟ้ารั่วหรือลัดวงจร ระบบจะตัดไฟเฉพาะส่วนที่เกิดเหตุ จึงสามารถใช้ไฟในส่วนอื่นๆ ของบ้านได้ ไม่เหมือนระบบเก่าที่ตัดไฟทั้งหลัง</li>
								<li>ปลั๊กไฟนิรภัย ปลั๊กไฟในบ้านทุกจุดคำนึงถึงการป้องกันอุบัติเหตุที่อาจเกิดจากเด็กเล็ก จึงมีม่านนิรภัยภายในปลั๊ก ป้องกันเด็กๆ เอานิ้วไปสัมผัส</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-2.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('safe-popup-2-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-3.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">ความปลอดภัยภายนอกบ้าน</div>
						<div class="desc">
							<p>
								ความสุขความผ่อนคลายเกิดขึ้นได้จากพื้นที่ภายนอกบ้าน ทุกดีไซน์ของพื้นที่ส่วนกลางจึงคำนึงถึงพฤติกรรม และการใช้งานที่ต้องปลอดภัย และสวยงาม
							</p>
							<ul>
								<li>พื้นที่สนามเด็กเล่นลดแรงกระแทก<br>เพราะสนามเด็กเล่น คือ พื้นที่ที่จะก่อให้เกิดทั้งความสนุก และความคิดสร้างสรรค์ของเด็กๆ เราจึงออกแบบให้พื้นสนามเด็กเล่นเป็นพื้นที่ช่วยลดแรงกระแทก เพื่อให้เด็กๆ เล่นสนุกได้อย่างมีความสุข และปลอดภัย</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-3.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('safe-popup-3-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-4.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">สระว่ายน้ำปลอดภัย</div>
						<div class="desc">
							<p>
								เพราะการว่ายน้ำเป็นการออกกำลังกายที่ดี และผ่อนคลาย การออกแบบสระว่ายน้ำจึงคำนึงถึงทั้งความปลอดภัย และสวยงาม ที่ทำให้มั่นใจได้ว่าผู้ใช้จะได้ทั้งสุขภาพที่ดี และความปลอดภัยในทุกครั้งที่ใช้บริการ
							</p>
							<ul>
								<li>แยกสระเด็ก สระผู้ใหญ่ รองรับการใช้งานให้เหมาะสม ออกแบบให้สระผู้ใหญ่สามารถมองเห็นสระเด็กได้โดยง่าย เพื่อให้เด็กๆ อยู่ในสายตาของคุณเสมอเมื่อว่ายน้ำ</li>
								<li>ดีไซน์ระดับความลึกของพื้นสระให้เสมอกันตลอดทั้งสระ ด้วยระดับความลึกที่สามารถยืนถึงจึงปลอดภัยแม้กับผู้ที่ว่ายน้ำไม่เป็น<br>
								   สระเด็ก : มีความลึกของพื้นระดับที่ 60 cm. เสมอกันตลอดทั้งสระ<br>
								   สระผู้ใหญ่ : มีความลึกของพื้นระดับที่ 120 cm. เสมอกันตลอดทั้งสระ</li>
								<li class="hide">สระว่ายน้ำเป็นระบบเกลือ จึงอ่อนโยนต่อผม ผิว และฟัน จึงไม่มีผลข้างเคียงเหมือนการใช้สารเคมีระบบเก่า</li>
								<li class="hide">มีเจ้าหน้าที่ประจำสระว่ายน้ำ ที่จะคอยดูแลให้ทุกคนอุ่นใจได้ในพื้นที่แห่งความสุขแห่งนี้</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-4.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('safe-popup-4-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-4-2.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">สระว่ายน้ำปลอดภัย</div>
						<div class="desc">
							<ul>
								<li>สระว่ายน้ำเป็นระบบเกลือ จึงอ่อนโยนต่อผม ผิว และฟัน จึงไม่มีผลข้างเคียงเหมือนการใช้สารเคมีระบบเก่า</li>
								<li>มีเจ้าหน้าที่ประจำสระว่ายน้ำ ที่จะคอยดูแลให้ทุกคนอุ่นใจได้ในพื้นที่แห่งความสุขแห่งนี้</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-4-1-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-5.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">อุ่นใจกับระบบรักษาความปลอดภัยมาตรฐาน Land & Houses</div>
						<div class="desc">
							<p>
								เพื่อให้คุณหลับสบายได้ทุกคืน และอุ่นใจในทุกวันที่ออกจากบ้านด้วยระบบรักษาความปลอดภัยมาตรฐาน แลนด์ แอนด์ เฮ้าส์ Triple Security System ที่พร้อมดูแลตลอด 24 ชม.
							</p>
							<ul>
								<li>ชั้นที่ 1 ระบบตรวจตราการเข้า-ออกหมู่บ้าน<br>จุด รปภ.ป้อมหน้า ทำหน้าที่คัดกรองผู้เข้า-ออก สำหรับผู้มาเยือนต้องแลกบัตร และแจ้งบ้านเลขที่ปลายทาง เมื่อผู้มาเยือนจะออกจากโครงการต้องมีตราประทับจากเจ้าของบ้าน และตรวจท้ายรถทุกครั้งก่อนออก</li>
								<li>ชั้นที่ 2 ระบบติดตามผู้มาเยือน<br> เจ้าหน้าที่รปภ.จะติดตามรถผู้มาเยือนจนถึงบ้านปลายทางที่แจ้งไว้</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-5.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('safe-popup-5-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="popup-content-section section">
		<div class="container popup-content-container">
			<div class="row">
				<div class="col-xs-4 left-col popup-content-col">
					<div class="title"><img src="<?php echo assets_images_url('safety-menu-text.png'); ?>" alt=""></div>
					<div class="image-panel" style="background-image: url('<?php echo assets_images_url('safety-6.jpg'); ?>');"></div>
				</div>
				<div class="col-xs-8 right-col popup-content-col">
					<div class="content-panel">
						<div class="title">อุ่นใจกับระบบรักษาความปลอดภัยมาตรฐาน Land & Houses</div>
						<div class="desc">
							<ul>
								<li>ชั้นที่ 3 บ้านทุกหลังติดตั้งสัญญาณนิรภัย<br>บ้านทุกหลังติดตั้งสัญญาณนิรภัยด้วยระบบ Magnetic System เมื่อมีผู้บุกรุกจะส่งสัญญาณเชื่อมผ่านโทรศัพท์ไปยังป้อม รปภ.หน้าหมู่บ้าน และมือถือเจ้าของบ้าน พร้อมไฟสัญญาณฉุกเฉินแจ้งเหตุหน้าบ้าน เพื่อปกป้องบ้าน และประสานเจ้าหน้าที่ รปภ. ให้มาดูแลอย่างทันท่วงที</li>
								<li>ทีมรปภ.ออกตรวจตราภายในโครงการตลอด 24 ชม. พร้อมระบบกล้อง CCTV</li>
							</ul>
						</div>
						<!-- <img src="<?php echo uploads_url('safe-popup-6.png'); ?>" class="hidden-xs">
						<img src="<?php echo uploads_url('safe-popup-6-mb.png'); ?>" class="visible-xs"> -->
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="images-playground" class="hide">
</div>

<div id="rotate-dialog">
	<div class="content-wrapper">
		<img src="<?php echo assets_images_url('icon-rotate-phone.gif'); ?>" alt="">
		<div class="text text-center">For a better experience.<br>Please rotate your device</div>
	</div>
</div>

<div id="nav-dialog" class="modal fade fullscreen" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close black" data-dismiss="modal" aria-label="Close"></button>

			<div id="nav-content-panel" class="container-fluid">
				<div class="text-center">
					<div class="title">How to visit 3d Museum</div>
				</div>
				<div class="row">
					<div class="col-xs-6 text-center" style="border-right: 2px solid #D9DADB;">
						<div class="hidden-xs">
							<img src="<?php echo assets_images_url('scroll-nav.png'); ?>" alt="">
							<div class="text">
								Scroll down to Next<br>
								Scroll up to Previous
							</div>
						</div>
						<div class="visible-xs">
							<img src="<?php echo assets_images_url('touch-nav.png'); ?>" alt="">
							<div class="text">
								Touch down to Next<br>
								Touch up to Previous
							</div>
						</div>
					</div>
					<div class="col-xs-6 text-center">
						<img class="hidden-xs" src="<?php echo assets_images_url('info-nav.png'); ?>" alt="">
						<img class="visible-xs" src="<?php echo assets_images_url('info-nav-mb.png'); ?>" alt="">

						<div class="text">
							Click this icon<br>
							for more information
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>