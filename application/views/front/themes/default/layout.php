<!DOCTYPE HTML>
<html>
    <head>
        <?php //header("Cache-Control: public, max-age=86400, s-maxage=120");?>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="robots" content="index,follow" />

		<?php
        /* Facebook Sharing */

        if(!isset($og_title) || !$og_title){
            $og_title = APP_NAME;
        }

        if(!isset($og_description) || !$og_description){
            $og_description = APP_DESCRIPTION;
        }

        if(!isset($og_image) || !$og_image){
            $og_image = assets_images_url('logo-square.png');
        }

        $fb_app_id = "720478081620850";
        ?>

        <meta property="fb:app_id"             content="<?php echo $fb_app_id; ?>" />
		<meta property="og:url"                content="<?php echo base_url(uri_string()); ?>" />
		<meta property="og:type"               content="website" />
		<meta property="og:title"              content="<?php echo htmlspecialchars($og_title); ?>" />
		<meta property="og:description"        content="<?php echo htmlspecialchars($og_description); ?>" />
		<meta property="og:image"              content="<?php echo $og_image; ?>" />
	    <meta property="og:site_name"		   content="<?php echo APP_NAME; ?>" />

        <meta name="title" content="<?php echo htmlspecialchars($og_title); ?>" />
        <meta name="description" content="<?php echo htmlspecialchars($og_description); ?>" />
        <meta name="keywords" content="index,follow,nosnippet" />

        <title><?php echo $title . ' | ' . APP_NAME; ?></title>

        <style>
            html,body,div,span,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,abbr,address,cite,code,del,dfn,em,img,ins,kbd,q,samp,small,strong,sub,sup,var,b,i,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,figcaption,figure,footer,header,hgroup,menu,nav,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent}body{line-height:1}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}nav ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:none}a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent}ins{background-color:#ff9;color:#000;text-decoration:none}mark{background-color:#ff9;color:#000;font-style:italic;font-weight:bold}del{text-decoration:line-through}abbr[title],dfn[title]{border-bottom:1px dotted;cursor:help}table{border-collapse:collapse;border-spacing:0}hr{display:block;height:1px;border:0;border-top:1px solid #ccc;margin:1em 0;padding:0}input,select{vertical-align:middle}
        </style>

        <?php
        for($i = 0; $i < count($css); $i++){
            echo $css[$i];
        }
        ?>

        <?php if(isset($canonical_url)){ ?>
        <link rel="canonical" href="<?php echo $canonical_url; ?>">
        <?php } ?>

        <script src="<?php echo assets_libs_url('modernizr-custom.min.js'); ?>"></script>

        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-121213394-2', 'auto'); ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-59XP889');</script>

        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NQRNBR');</script>
        <!-- End Google Tag Manager -->
    </head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-59XP889"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQRNBR"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '720478081620850',
                xfbml      : true,
                version    : 'v3.1'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <div id="body-wrapper">
            <?php echo $header; ?>

            <div id="main-content-panel">
                <?php echo $body; ?>

                <div id="item-loading-panel">
                    <img src="<?php echo assets_images_url('item-loading.gif'); ?>">
                </div>
            </div>

            <?php echo $footer; ?>
        </div>

        <div id="loading-panel" class="circle">
            <div class="spinner loading-icon"></div>
        </div>

        <div id="message-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6"><?php echo APP_NAME; ?></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer text-center-force">
                        <button type="button" id="ok-button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="confirm-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6"><?php echo APP_NAME; ?></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="confirm-button" class="btn btn-primary btn-yellow-1">Confirm</button>
                        <button type="button" id="cancel-button" data-dismiss="modal" class="btn btn-primary btn-grey-1">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        for($i = 0; $i < count($js); $i++){
            echo $js[$i];
        }
        ?>
    </body>
</html>