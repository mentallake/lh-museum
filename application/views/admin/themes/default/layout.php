<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="robots" content="index,follow" />

		<?php
        /* Facebook Sharing */

        if(!isset($og_title) || !$og_title){
            $og_title = APP_NAME;
        }

        if(!isset($og_description) || !$og_description){
            $og_description = APP_DESCRIPTION;
        }

        if(!isset($og_image) || !$og_image){
            // $og_image = assets_images_url('logo-square.png');
        }

        $fb_app_id = "";
        ?>

        <meta property="fb:app_id"             content="<?php echo $fb_app_id; ?>" />
		<meta property="og:url"                content="<?php echo base_url(uri_string()); ?>" />
		<meta property="og:type"               content="website" />
		<meta property="og:title"              content="<?php echo htmlspecialchars($og_title); ?>" />
		<meta property="og:description"        content="<?php echo htmlspecialchars($og_description); ?>" />
		<meta property="og:image"              content="<?php echo $og_image; ?>" />
	    <meta property="og:site_name"		   content="<?php echo APP_NAME; ?>" />

        <meta name="title" content="<?php echo htmlspecialchars($og_title); ?>" />
        <meta name="description" content="<?php echo htmlspecialchars($og_description); ?>" />
        <meta name="keywords" content="index,follow,nosnippet" />

        <title><?php echo $title . ' | ' . APP_NAME; ?></title>

        <?php
        for($i = 0; $i < count($css); $i++){
            echo $css[$i];
        }
        ?>

        <?php if(isset($canonical_url)){ ?>
        <link rel="canonical" href="<?php echo $canonical_url; ?>">
        <?php } ?>

        <script src="<?php echo assets_libs_url('modernizr-custom.min.js'); ?>"></script>
    </head>

    <body>
        <div id="body-wrapper">
            <?php echo $header; ?>

            <div id="main-content-panel">
                <?php echo $body; ?>

                <div id="item-loading-panel">
                    <img src="<?php echo assets_images_url('item-loading.gif'); ?>">
                </div>
            </div>

            <?php echo $footer; ?>
        </div>

        <div id="loading-panel" class="circle">
            <div class="spinner loading-icon"></div>
        </div>

        <div id="message-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6"><?php echo APP_NAME; ?></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer text-center-force">
                        <button type="button" id="ok-button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="confirm-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6"><?php echo APP_NAME; ?></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="confirm-button" class="btn btn-primary btn-yellow-1">Confirm</button>
                        <button type="button" id="cancel-button" data-dismiss="modal" class="btn btn-primary btn-grey-1">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        for($i = 0; $i < count($js); $i++){
            echo $js[$i];
        }
        ?>
    </body>
</html>