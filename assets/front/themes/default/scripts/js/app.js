$(function(){
	initAnimateCss();
});

function initAnimateCss(){
	$.fn.extend({
	    animateCss: function (animationName, type) {
	    	if(type == 'in'){
            	$(this).removeClass('hide');
            	$(this).css('opacity', '1');
            }

	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);

	            if(type == 'out'){
	            	$(this).css('opacity', '0');
	            	$(this).addClass('hide');
	            }
	        });
	    }
	});
}

function showLoadingPanel(){
    $('#loading-panel').show();
}

function hideLoadingPanel(){
    $('#loading-panel').hide();
}