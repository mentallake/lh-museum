var maxScene = 14;
var totalImages = 503;
var bubbleTimeout = false;
var rewindTimeInterval = false;
var loadingTimeInterval = false;
// var transitionSceneImageIndex = [
// 							[675, 700], 	// Enter room 1
// 							[1, 60], 		// Enter room 2
// 							[61, 72], 		// Bring 1st bubble to front
// 							[79, 90], 		// Bring 2nd bubble to front
// 							[99, 110], 		// Bring 3rd bubble to front
// 							[119, 191], 	// Enter room 3 - Front view
// 							[194, 250], 	// Rroom 3 - Side view
// 							[269, 350], 	// Enter room 4
// 							[350, 379], 	// Room 4 - Expand house
// 							[380, 446], 	// Enter room 5 - Front
// 							[458, 530], 	// Room 5 - Side
// 							[561, 700], 	// Enter room 1
// 						];
var transitionSceneImageIndex = [
							[491, 500], 	// Enter room 1
							[500, 502],
							[501, 502],
							[0, 70], 		// Enter room 2
							[69, 82], 		// Bring 1st bubble to front
							[90, 101], 		// Bring 2nd bubble to front
							[110, 121], 	// Bring 3rd bubble to front
							[130, 211], 	// Enter room 3 - Front view
							[220, 241], 	// Rroom 3 - Side view
							// [130, 241], 	// Rroom 3
							[250, 314], 	// Enter room 4
							[320, 341], 	// Room 4 - Expand house
							// [250, 341], 	// Room 4
							[350, 391], 	// Enter room 5 - Front
							[400, 429], 	// Room 5 - Side
							// [350, 429], 	// Room 5 - Side
							// [350, 429], 	// Enter room 5
							[430, 491], 	// Enter room 1
						];
var lastScrollIndex = 0;
var startFrame = false;
var endFrame = false;
var infoContentFullPage = false;
var height = 0;
var isRunThrough = false;
var canvas = document.getElementById('canvas');
var canvasContext = canvas.getContext("2d");

$(function(){
	initMenuPanel();
	initNavButtons();
	initInfoPoints();

	canvas.width = $(canvas).width();
    canvas.height = $(canvas).height();

	$('#enter-btn').click(function(){
		// showLoadingPanel();

		$(this).hide();
		$('.progress-panel').show();

		loadingTimeInterval = setInterval(function(){
			if(!$('body').hasClass('loading')){
				clearInterval(loadingTimeInterval);
				$('#enter-museum-panel').hide();
				$('#logo').removeClass('hide');
				$('#main-menu-btn').removeClass('hide');
				initScroll();
				hideLoadingPanel();

				$('#scene-container').addClass('showing-popup');

				$("#nav-dialog").on('hidden.bs.modal', function(){
					$('#scene-container').removeClass('showing-popup');

					// startFrame = 1;
					// endFrame = 500;

					// $('#current-frame').val(startFrame);

					// rewindTimeInterval = setInterval(function(){
					// 	var scene = $('#current-scene').val();
					// 	var frame = $('#current-frame').val();
					// 	frame = parseInt(frame) + 1;

					// 	if(frame == endFrame){
					// 		clearInterval(rewindTimeInterval);
					// 		$('.nav-container .nav-btn').removeClass('disabled');

					// 		showSceneInfo(scene);
					// 		return;
					// 	}

					// 	var frameIndex = frame - 1;
					// 	var imageUrl = $('#images-playground > img').eq(frameIndex).attr('src');
					// 	// console.log("Image Url", imageUrl);

				 //        $('#scene-container').css('background-image', 'url(' + imageUrl + ')');
				 //        $('#current-frame').val(frame);
					// }, 40);

					// for(var i = 0; i < $('#images-playground > img').length; i++){
					// 	var imageUrl = $('#images-playground > img').eq(i).attr('src');
					// 	console.log("Image Url", imageUrl);

				 //        $('#scene-container').css('background-image', 'url(' + imageUrl + ')');
					// }
				});

				$('#nav-dialog').modal();
			}
		}, 400);
	});

	$('#share-fb-btn').click(function(){
		ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'share to facebook'});

		FB.ui({
		  	method: 'share',
		  	href: 'http://www.google.com/',
		}, function(response){});
	});

	$('#share-line-btn').click(function(){
		ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'share to line'});
	});

	$('#share-tw-btn').click(function(){
		ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'share to twitter'});

		var url = "http://google.com";
		var text = "LH Musuem";
		window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
	});

	$('#lh-logo').click(function(){
		ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'back to web lh'});

		var href = $(this).attr('data-href');
		window.location.href = href;
	});

	$('#contact-btn').click(function(){
		var currentScene = parseInt($('#current-scene').val());
		var href = $(this).attr('data-href');

		if(currentScene == 6){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'contact us health room'});
		}else if(currentScene == 7){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'contact us convenience room'});
		}else if(currentScene == 8){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'click', eventLabel: 'contact us safety room'});
		}

		window.location.href = href;
	});

	// Destroy fullpage script
	$('#point-info-dialog').on('hidden.bs.modal', function(){
		$.fn.fullpage.destroy('all');
	});

	loadImages();

	$("#rotate-dialog").on('hidden.bs.modal', function(){
		$('#scene-container').removeClass('showing-popup');
	});

	$(window).scroll(function(e){
		$(window).scrollTop(-1);
		e.preventDefault();
	});

	// if(window.innerHeight != $(window).height()){
	// 	var a = setInterval(function() {
	//         $(window).scrollTop(-1);
	//         handleWindowResize();
	//     }, 500); // Don't lower more than 500ms, otherwise there will be animation-problems with the  Safari toolbar
	// }

	var a = setInterval(function() {
	        // $(window).scrollTop(-1);
	        handleWindowResize();
	    }, 500); // Don't lower more than 500ms, otherwise there will be animation-problems with the  Safari toolbar

    // Listen for resize changes
	$(window).resize(function(){
	  	handleWindowResize();
	});

	$(window).resize();

	$("#image").css('opacity', 1);
});

function handleWindowResize(){
	$(window).scrollTop(-1);

  	var windowWidth = $(window).width();
  	var windowHeight = $(window).height();
  	var innerHeight = window.innerHeight;

  	canvas.width = $(canvas).width();
    canvas.height = $(canvas).height();

    // if(innerHeight < 331){
    // 	var fontSize = '2.1vw';
    // 	$('#scene-container .scene-text .desc').css('font-size', fontSize);
    // 	$('.popup-content-section .popup-content-col .desc').css('font-size', fontSize);
    // }else if(innerHeight < 414){
    // 	var fontSize = '2.3vw';
    // 	$('#scene-container .scene-text .desc').css('font-size', fontSize);
    // 	$('.popup-content-section .popup-content-col .desc').css('font-size', fontSize);
    // }else{
    // 	$('#scene-container .scene-text .desc').css('font-size', "");
    // 	$('.popup-content-section .popup-content-col .desc').css('font-size', "");
    // }
    // if(innerHeight <= 414 || windowWidth <= 992){
    // 	var fontSize = '20px';
    // 	var titleFontSize = '24px';
    // 	$('#scene-container .scene-text .title').css('font-size', '14px');
    // 	$('#scene-container .scene-text .desc').css('font-size', fontSize);
    // 	$('.popup-content-section .popup-content-col .title').css('font-size', titleFontSize);
    // 	$('.popup-content-section .popup-content-col .desc').css('font-size', fontSize);
    // }else{
    // 	$('#scene-container .scene-text .title').css('font-size', "");
    // 	$('#scene-container .scene-text .desc').css('font-size', "");
    // 	$('.popup-content-section .popup-content-col .title').css('font-size', "");
    // 	$('.popup-content-section .popup-content-col .desc').css('font-size', "");
    // }

    if(innerHeight == 360 && windowWidth == 740){ // Specific for Samsung S8
    	var titleFontSize = '20px';
    	var fontSize = '18px';

    	// $('#scene-container .scene-text .title').css('font-size', '14px');
    	// $('#scene-container .scene-text .desc').css('font-size', fontSize);
    	$('.popup-content-section .popup-content-col .title').css('font-size', titleFontSize);
    	$('.popup-content-section .popup-content-col .desc').css('font-size', fontSize);
    }else{
    	// $('#scene-container .scene-text .title').css('font-size', "");
    	// $('#scene-container .scene-text .desc').css('font-size', "");
    	$('.popup-content-section .popup-content-col .title').css('font-size', "");
    	$('.popup-content-section .popup-content-col .desc').css('font-size', "");
    }

  	if(innerHeight != height) {
        height = innerHeight;

        $('#showroom-container').css('height', height + 'px');
        $('#scene-container').css('height', height + 'px');
        $('#image').css('height', height + 'px');
        $('.popup-content-section').css('height', height + 'px');
        $('#enter-museum-panel').css('height', height + 'px');
        $('body').css('height', height + 'px');
        $('#nav-dialog .modal-content').css('height', height + 'px');

        if($(window).width() < 1024){
        	$('#menu-panel').css('height', height + 'px');
        }else{
        	$('#menu-panel').css('height', '100%');
        }

        // $('#point-info-dialog .popup-content-section').css('height', height + 'px');

  		$('#console-panel').prepend(height + ":" + innerHeight + "\n");
    }

    $('#showroom-container').css('height', innerHeight + 'px');
    $('#scene-container').css('height', innerHeight + 'px');
    $('#image').css('height', innerHeight + 'px');
    $('.popup-content-section').css('height', innerHeight + 'px');
    $('#enter-museum-panel').css('height', innerHeight + 'px');
    $('body').css('height', innerHeight + 'px');
    $('#nav-dialog .modal-content').css('height', innerHeight + 'px');

    // $('#nav-dialog .modal-content .title').text(height);
    // $('#enter-btn').text(window.innerHeight + ":" + height);

  	if(windowWidth > windowHeight){
  		// Landscape
  		$('#rotate-dialog').hide();
  	}else{
  		// Portrait
  		$('#rotate-dialog').show();
  	}
}

function enterScene(scene, isAutoSlide){
	isAutoSlide = typeof isAutoSlide !== 'undefined' ? isAutoSlide : false;

	// Adjust scene
	if(scene <= 0){
		scene = maxScene - 1;

		$('#current-scene').val(maxScene);
	}else if(scene == 1){
		var isFromMenu = Math.abs(currentScene - scene) > 1;

		if(isFromMenu){
			$('#current-scene').val(0);
		}
	}else if(scene > maxScene){
		scene = 2;

		// To make the scene move forward, need to reset the current scene to 0.
		// Trick the flow as start from begining.
		$('#current-scene').val(1);
	}

	var currentScene = parseInt($('#current-scene').val());
	var interval = false;
	var direction = currentScene < scene ? 'next' : 'prev';
	var sceneIndex = scene - 1;

	// Fire google analytic
	if(scene == 1 && currentScene > 1){
		ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'back to \'create a better living room\''});
	}

	if(scene == 4){
		if(currentScene > 4){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'back to bubble room'});
		}else if(currentScene < 4){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'go to bubble room'});
		}
	}

	if(scene == 8){
		if(currentScene > 8){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'back to health room'});
		}else if(currentScene < 8){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'go to health room'});
		}
	}

	if(scene == 10){
		if(currentScene > 10){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'back to convenience room'});
		}else if(currentScene < 10){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'go to convenience room'});
		}
	}

	if(scene == 12){
		if(currentScene > 12){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'back to safety room'});
		}else if(currentScene < 12){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'go to safety room'});
		}
	}

	if(scene == 14){
		if(currentScene > 14){
			// ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'back to \'create a better living room\''});
		}else if(currentScene < 14){
			ga('send', 'event', { eventCategory: 'museum', eventAction: 'scroll', eventLabel: 'go to \'create a better living room\''});
		}
	}

	if(currentScene == scene){
		console.log("Scene " + currentScene + " --> " + scene);
		return;
	}

	if($('#room-menus > li > a[data-target="' + scene +'"]').length > 0){
		$('#room-menus > li').removeClass('active');
		$('#room-menus > li > a[data-target="' + scene +'"]').parent().addClass('active');
	}

	if(scene == maxScene){
		$('#room-menus > li').removeClass('active');
		$('#room-menus > li > a[data-target="' + 1 +'"]').parent().addClass('active');
	}else if(scene == maxScene - 1){
		$('#room-menus > li').removeClass('active');
		$('#room-menus > li > a[data-target="' + 12 +'"]').parent().addClass('active');
	}

	$('#scene-container').addClass('animating');

	if(!isAutoSlide){
		clearTimeout(bubbleTimeout);
		isRunThrough = false;
	}

	if(scene == 1){
		$('#logo').addClass('hide');
	}else{
		if($('#logo').hasClass('hide')){
			$('#logo').animateCss('fadeIn', 'in');
		}

		if(!$('#scroll-icon').hasClass('hide')){
			$('#scroll-icon').hide();
		}
	}

	$('.scene').hide();

	console.log("Scene " + currentScene + " --> " + scene);
	$("#console-panel").prepend("Scene " + currentScene + " --> " + scene + "\n");

	$('.nav-container .nav-btn').addClass('disabled');

	// Check scene text if need to hide before animating.
	checkToHideSceneText(scene);

	if(direction == 'next'){
		// Set start and end frame.
		startFrame = transitionSceneImageIndex[sceneIndex][0];
		endFrame = transitionSceneImageIndex[sceneIndex][1];

		if(scene == 2){
			if(currentScene < 2){
				isRunThrough = true;
				// endFrame = transitionSceneImageIndex[4][1];
			}
		}

		$('#current-frame').val(startFrame);

		rewindTimeInterval = setInterval(function(){
			var scene = $('#current-scene').val();
			var frame = $('#current-frame').val();
			frame = parseInt(frame);

			var frameIndex = frame;
			var imageUrl = $('#images-playground > img').eq(frameIndex).attr('src');
			// console.log("Image Url", imageUrl);

	        $('#scene-container').css('background-image', 'url(' + imageUrl + ')');

	        var w = canvas.width;
        	var h = canvas.height;

        	var img = $('#images-playground > img').eq(frameIndex)[0];
        	var newSizes = scaleSize(w, h, img.width, img.height);

        	// console.log(newSizes);

	        canvasContext.drawImage(img, (w-newSizes.width)/2, (h-newSizes.height)/2, Math.floor(newSizes.width), Math.floor(newSizes.height));

	        if(frame == endFrame){
				clearInterval(rewindTimeInterval);
				$('.nav-container .nav-btn').removeClass('disabled');

				if(scene != 7 && isRunThrough){
					showSceneInfo(scene, isRunThrough);

					$('#current-scene').val(scene);

					console.log('current scene', scene);

					var interval = 1500;

					if(scene <= 3){
						interval = 10000;
					}else if(scene <= 7){
						interval = 5000;
					}

					bubbleTimeout = setTimeout(function(){
						var nextScene = parseInt(scene) + 1;
						console.log('enter scene', nextScene);
						enterScene(nextScene, true);
					}, interval);
				}else{
					isRunThrough = false;

					showSceneInfo(scene, isRunThrough);
				}

				return;
			}

			$('#current-frame').val(++frame);
		}, 40);

		$('#current-scene').val(scene);
	}else{
		sceneIndex++;

		// Set start and end frame.
		startFrame = transitionSceneImageIndex[sceneIndex][1];
		endFrame = transitionSceneImageIndex[sceneIndex][0];

		$('#current-frame').val(startFrame);

		rewindTimeInterval = setInterval(function(){
			var scene = $('#current-scene').val();
			var frame = $('#current-frame').val();
			frame = parseInt(frame);

			var frameIndex = frame;
			var imageUrl = $('#images-playground > img').eq(frameIndex).attr('src');

			$('#scene-container').css('background-image', 'url(' + imageUrl + ')');

			var w = canvas.width;
        	var h = canvas.height;

        	var img = $('#images-playground > img').eq(frameIndex)[0];
        	var newSizes = scaleSize(w, h, img.width, img.height);

        	// console.log(newSizes);

	        canvasContext.drawImage(img, (w-newSizes.width)/2, (h-newSizes.height)/2, Math.floor(newSizes.width), Math.floor(newSizes.height));

	        if(frame == endFrame){
				clearInterval(rewindTimeInterval);
				$('.nav-container .nav-btn').removeClass('disabled');

				showSceneInfo(scene, isRunThrough);

				return;
			}

	        // $('#scene-container').css('background-image', 'url(' + imageUrl + ')');
	        $('#current-frame').val(--frame);
		}, 40);

		$('#current-scene').val(scene);
	}
}

function scaleSize(maxW, maxH, currW, currH){
    var ratioCurr = currW / currH;
    var ratioMax = maxW / maxH;

    if( ratioMax > ratioCurr ){
        currW = maxW;
        currH = Math.floor(maxW / ratioCurr);
    } else {
        currH = maxH;
        currW = Math.floor(maxH * ratioCurr);
    }

    return {width: currW, height: currH};
}

function initScroll(){
	$('#scene-container > section').horizon({
		fnCallback: function (i) {
			console.log("Callback", i);
			console.log("Callback", lastScrollIndex);
			$("#console-panel").prepend(">> Callback " + i + "\n");
			$("#console-panel").prepend(">> lastScrollIndex " + lastScrollIndex + "\n");

			if($('#scene-container').hasClass('animating') ||
			   $('#scene-container').hasClass('showing-popup')){
			   	lastScrollIndex = i;
				return;
			}

			var currentScene = parseInt($('#current-scene').val());
			var toScene = currentScene;

			if(lastScrollIndex == maxScene - 1){
				if(i == 0){
					// Scroll down
					toScene++;
				}else{
					// Scrool up
					toScene--;
				}
			}else if(lastScrollIndex == 0){
				if(i == maxScene - 1){
					// Scroll up
					toScene--;
				}else{
					// Scrool down
					toScene++;
				}
			}else{
				if(i > lastScrollIndex){
					// Scroll down
					toScene++;
				}else if(i < lastScrollIndex){
					// Scrool up
					toScene--;
				}
			}

			lastScrollIndex = i;

			// console.log("To Scene", toScene);
			// $("#console-panel").prepend(">> Start Enter Room " + toScene + "\n");

			enterScene(toScene);
		}
	});
}

function checkToHideSceneText(scene){
	var currentStartScene = parseInt($('.scene-text.active').attr('data-start'));
	var currentEndScene = parseInt($('.scene-text.active').attr('data-end'));

	if(currentEndScene < scene || currentStartScene > scene){
		$('.scene-text.active').removeClass('active');

		// Hide scroll
		// $('.popup-scroll-icon').css('opacity', '0');
		// $('#begin-scroll-icon').css('opacity', '0');
	}
}

function showSceneInfo(scene, isRunThrough){
	$('#scene-' + scene).show();

	if(scene >= 2){
		$('.popup-scroll-icon').css('opacity', '1');
		$('#begin-scroll-icon').css('opacity', '0');
	}else{
		$('.popup-scroll-icon').css('opacity', '0');
		$('#begin-scroll-icon').css('opacity', '1');
	}

	showInfoPoint(scene);
	showText(scene);

	if(isRunThrough){
		$('#scene-container').removeClass('animating');
	}else{
		$('#scene-container').removeClass('animating');
	}

	$('#alert-message').hide();
}

function showText(scene){
	$('.scene-text[data-start="' + scene + '"]').each(function(){
		var startScene = $(this).attr('data-start');
		var endScene = $(this).attr('data-end');
		$(this).addClass('active');
	});

	$('.scene-text[data-end="' + scene + '"]').each(function(){
		var startScene = $(this).attr('data-start');
		var endScene = $(this).attr('data-end');
		$(this).addClass('active');
	});
}

function showInfoPoint(scene){
	$('#scene-' + scene + ' .info-icon').each(function(){
		var animateIn = $(this).attr('data-animate-in');
		var x = $(this).attr('data-position-x');
		var y = $(this).attr('data-position-y');

		$(this).css({
			left: x,
			top: y,
		});

		$(this).animateCss(animateIn, 'in');
	});
}

function initInfoPoints(){
	$("#point-info-dialog").on('hidden.bs.modal', function(){
		$("#console-panel").prepend("Dismiss Popup\n");
		$('#scene-container').removeClass('showing-popup');
	});

	$("#point-info-dialog .close").click(function(){
		$("point-info-dialog").modal('hide');
	});

	$('.info-icon').click(function(){
		$('#scene-container').addClass('showing-popup');
		$("#console-panel").prepend("Show Popup\n");

		var sceneType = $(this).attr('data-scene');
		var contentHtml = false;

		switch(sceneType){
			case 'convenience':
				contentHtml = $('#convenience-popup-content').html();
				break;
			case 'health':
				contentHtml = $('#health-popup-content').html();
				break;
			case 'safety':
				contentHtml = $('#safety-popup-content').html();
				break;
		}

		if(contentHtml){
			$('#point-info-dialog #info-content-panel').html(contentHtml);

			var totalSection = $('#point-info-dialog #info-content-panel .section').length;
			var anchors = [];

			for(var i = 0; i < totalSection; i++){
				anchors.push("Page-" + (i + 1));
			}

			var isMobile = $(window).width() <= 812;

			if(!isMobile){
				$('#point-info-dialog #info-content-panel .mobile-section').remove();
			}

			$('#point-info-dialog #info-content-panel').fullpage({
				menu: '#menu',
				anchors: anchors,
				navigation: true,
				fitToSection: true,
				navigationPosition: 'right',
				afterLoad: function(origin, destination){
					var maxHeight = height - 100;
					$('.popup-content-section').css('height', height+ 'px');
					$('.fp-tableCell').css('height', height + 'px');
					$('.popup-content-section .content-panel img').css('max-height', maxHeight+ 'px');

					if(window.innerHeight < $(window).height()){
						var offsetTop = 0 - ((destination - 1) * height);

						$('#info-content-panel').css({'transition': 'all 300ms ease'});
						$('#info-content-panel').css({'transform': 'translate3d(0px, ' + offsetTop + 'px, 0px)'});

						var offsetContactTop = (height - $('#contact-btn').height()) - 10;
						$('#contact-btn').css('top', offsetContactTop + 'px');
						$('#contact-btn').css('bottom', 'auto');
					}

					// Calculate max height of fp-nav > ul > li
					var totalSlides = $('#fp-nav > ul > li').length;
					var maxHeightButton = (window.innerHeight - 120) / totalSlides;
					var windowHeight = window.innerHeight;
					var navHeight = $('#fp-nav > ul').outerHeight();
					$('#fp-nav > ul').css('top', '0px');
					// $('#fp-nav > ul').css('margin-top', '-' + navHeight / 2 + 'px');
					$('#fp-nav > ul > li').css('max-height', maxHeightButton + 'px');
				},
				onLeave: function(index, nextIndex, direction){
					console.log(index);

					var isFirstSlide = nextIndex == 1;
			    	var isLastSlide = nextIndex == $('#point-info-dialog .popup-content-section').length;

			    	$('#contact-btn').hide();
			    	$('#popup-scroll-icon').removeClass('first last');

					if(isLastSlide){
						$('#contact-btn').fadeIn();
						$('#popup-scroll-icon').addClass('last');
					}else if(isFirstSlide){
						$('#popup-scroll-icon').addClass('first');
					}else{
						$('#contact-btn').fadeOut();
					}
			    },
			    afterRender: function(){

				},
				afterResize: function(width, height){
					var targetHeight = height - 100;
					console.log(targetHeight);
				}
			});

			$.fn.fullpage.moveTo(1);
		}

		$('#point-info-dialog .popup-content-col').matchHeight();

		$("#point-info-dialog").modal();
	});
}

function initNavButtons(){
	$('.nav-container .nav-btn.prev').click(function(){
		if($(this).hasClass('disabled'))
			return;

		var currentScene = parseInt($('#current-scene').val());
		var toScene = currentScene - 1;

		enterScene(toScene);
	});

	$('.nav-container .nav-btn.next').click(function(){
		if($(this).hasClass('disabled'))
			return;

		var currentScene = parseInt($('#current-scene').val());
		var toScene = currentScene + 1;

		enterScene(toScene);
	});
}

function initMenuPanel(){
	$('#main-menu-btn').click(function(){
		$(this).hide();
		$('#menu-panel').animateCss('fadeIn', 'in');
		$('#scene-container').addClass('showing-popup');
	});

	$('#close-menu-btn').click(function(){
		if(!$('#scene-container').hasClass('showing-popup') || $('#main-menu-btn:visible').length > 0){
			return false;
		}

		$('#menu-panel').animateCss('fadeOut', 'out');
		$('#scene-container').removeClass('showing-popup');
		$('#main-menu-btn').show();
	});

	$('#menus > li > a').click(function(e){
		var targetScene = $(this).attr('data-target');

		if($('#scene-container').hasClass('animating')){
		   	// $('#alert-message').show();
		   	e.preventDefault();
		   	return false;
		}

		if($('#scene-container').hasClass('showing-popup')){
			$("#point-info-dialog").modal('hide');
		}

		$('#alert-message').hide();
		$('#close-menu-btn').click();

		enterScene(targetScene);
	});

	$('#room-menus > li > a').click(function(e){
		var targetScene = $(this).attr('data-target');

		if($('#scene-container').hasClass('animating')){
		   	// $('#alert-message').show();
		   	e.preventDefault();
		   	return false;
		}

		if($('#scene-container').hasClass('showing-popup')){
			$("#point-info-dialog").modal('hide');
		}

		$('#alert-message').hide();
		$('#close-menu-btn').click();

		enterScene(targetScene);
	});

	$(document).on('click', '#scene-container', function(){
		if($(this).hasClass('showing-popup')){
			$('#close-menu-btn').click();
		}
	});
}

function loadImages(){
	// showLoadingPanel();
	if($("#images-playground img").length == totalImages){
		$('body').removeClass('loading');
		// enterScene(1);
		$("#image").css('opacity', 1);
		return;
	}

	var selectedComponentIds = [];

	$('#customize-panel .component-list').each(function(){
		var iId = $(this).find('li.active').attr('data-id');

		selectedComponentIds.push(iId);
	});

	var url = site_url + "/home/get_images";

	$.ajax({
		url: url,
		method: "POST",
		data: { },
		dataType: "json",
		cache: true
	}).done(function(response){
		var data = response.data;
		var totalImages = response.data.length;
		var loadedCount = 0;

		// console.log("Total Images", totalImages);

		$("#images-playground").html('');

		for(var i = 0; i < totalImages; i++){
			$('body').addClass('loading');

			var imageName = data[i]['name'];
			var imageUrl = uploads_url + "sq-images/" + imageName;
			var img = new Image;

			img.onload = function(e) {
				loadedCount++;

				// originalImageWidth = e.currentTarget.width;

				var percent = loadedCount / totalImages * 100;
				$('.progress-bar').css('width', percent + '%');
				$('.progress-text').text(percent.toFixed(0) + '%');

				if(loadedCount == totalImages){
					if(totalImages > 0){
						$('body').removeClass('loading');
						// enterScene(1);
						$("#image").css('opacity', 1);

						// $('#nav-dialog').modal();

						// initScroll();
					}

					hideLoadingPanel();
				}
			};

			img.onerror = function(e) {
				loadedCount++;

				if(loadedCount == totalImages){
					if(totalImages > 0){
						$('body').removeClass('loading');
						// enterScene(1);
						$("#image").css('opacity', 1);

						// initScroll();
					}

					hideLoadingPanel();
				}
			};

			img.src = imageUrl;

			$("#images-playground").append('<img src="' + imageUrl + '">');
		}
	});
}